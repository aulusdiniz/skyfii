#!/usr/bin/python3

import pymysql
import pandas as pd
import numpy as np
import pdb
import os
from os import listdir
from os.path import isfile, join
import shutil

from functions import *

# ALTER TABLE users ADD COLUMN address varchar(10);
# DESCRIBE users;


os.chdir('../unprocessed')

print('Script rodando')

while True:
    for folder in os.listdir():
        os.chdir(os.getcwd() + '/' + folder)

        # print("folder being processed now: ", folder)

        onlyfiles = [f for f in listdir(os.getcwd()) if isfile(join(os.getcwd(), f))]
        for fileInDirectory in onlyfiles:
            if fileInDirectory != 'data.txt':
                if tableExists(folder):
                    updateTableIfExists(folder, fileInDirectory, os.getcwd())
                else:
                    createTable(folder, fileInDirectory)
                csvPath = os.getcwd() + '/' + fileInDirectory
                addDataToTable(getData(csvPath), folder, csvPath)
                shutil.move(fileInDirectory, "../../processed/"+folder+"/"+fileInDirectory)
        os.chdir('../../unprocessed')

print('script parou')
