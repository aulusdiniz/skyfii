import pandas as pd
import numpy as np
import pymysql
import pdb
from csv import reader
import json
import os
from os import listdir
from os.path import isfile, join


#### Funções para Banco de Dados

def getArrayWithFirstRowAndSecondRowLenght(csvPath):
    # open file in read mode
    with open(csvPath, 'r') as read_obj:
        firstRow            = []
        lenghtOfSecondRow   = []
        i = j = 0
        # pass the file object to reader() to get the reader object
        csv_reader = reader(read_obj)
        # Iterate over each row in the csv using reader object
        for row in csv_reader:
            if i == 2:
                break
            # row variable is a list that represents a row in csv
            for element in row:
                if i == 1:
                    lenghtOfSecondRow.append(len(element))
                elif i == 0:
                    firstRow.append(element)
                j += 1
            i += 1
        firstRow            = np.array(firstRow)
        lenghtOfSecondRow   = np.array(lenghtOfSecondRow)
        array               = np.stack((firstRow, lenghtOfSecondRow), axis=-1)
        return array

def createTable(tableName, csvPath):
    schema = createTableSchema(csvPath , tableName)
    connection  = pymysql.connect(host='localhost',
                             user='root',
                             password='powerfi',
                             db='db',
                             charset='utf8mb4'
                            )
    try:
        with connection.cursor() as cursor:
            # cria tabela
            cursor.execute(schema)
            connection.commit()
    finally:
        connection.close()
        saveJsonWithColumns(getFirstRow(csvPath), tableName)

def addDataToTable(data, tableName, csvPath):
    df              = getDataFrame(data, csvPath)
    
    insertSchema    = createInsertSchema(csvPath, tableName)
    connection  = pymysql.connect(host='localhost',
                             user='root',
                             password='powerfi',
                             db='db',
                             charset='utf8mb4'
                            )
    try:
        with connection.cursor() as cursor:
            # Insert DataFrame to Table
            for row in df.itertuples():
                rowData = [len(row)]
                i=0
                csvFile = str(csvPath).split('/')
                csvFile = csvFile[len(csvFile)-1]
                rowData.append(csvFile)
                for element in row:
                    if i != 0: 
                        rowData.append(element)
                    if i == 1 and element == 'null':
                        break
                    i += 1
                rowData.pop(0)
                rowData = tuple(rowData)
                #sql = ', '.join(map(str, rowData))
                if rowData[1] != 'null':
                    cursor.execute(insertSchema, rowData)
            connection.commit()
    except pymysql.err.OperationalError as error:
        print(error)
        pdb.set_trace()
    finally:
        connection.close()

# tableName é o nome da pasta onde o arquivo está salvo
def createTableSchema(csvPath, tableName):
    tupla = getArrayWithFirstRowAndSecondRowLenght(csvPath)
    schema = 'CREATE TABLE ' + tableName + ' ( csvPath nvarchar(100) '
    i = 0
    for row in tupla:
        # pega o nome da coluna e o tamanho máximo do campo
        maxLenght = int(row[1]) * 2
        if maxLenght < 25:
            maxLenght = 1000
        elif maxLenght > 200:
            maxLenght = 3000
        schema += ', ' + row[0]  + ' nvarchar('    + str(round(maxLenght)) + ')'
        i += 1
    schema += ')'
    return schema

def createInsertSchema(csvPath, tableName):
    header = getFirstRow(csvPath)
    schema = '''INSERT INTO ''' + tableName + ''' ( csvPath'''
    values = "VALUES (%s"
    # values = "VALUES ('{" + str(0) + "}'"
    i = 0
    for element in header:
        schema += ',' + element
        # if element == 'nationalId':
        #     pdb.set_trace()
        values += ", %s"
        # values += ", '{" + str(i+1) + "}'" 
        i += 1
    schema += ') '
    values += ')'
    return schema + values


def killTableIfExists(tableName):
    connection = pymysql.connect(host='localhost',
                             user='root',
                             password='powerfi',
                             db='db',
                             charset='utf8mb4'
                            )
    try:
        with connection.cursor() as cursor:
            # Delete table with tableName
            cursor.execute("DROP TABLE  IF EXISTS "+ tableName +";")
            # result = cursor.fetchone()
            # if result:
            #     cursor.execute('DROP TABLE '+ tableName + ";")

            connection.commit()
    finally:
        connection.close()

def updateTableIfExists(folder, firstCSVName, pathToFolder):
    columnsFromFirstCSV =    getFirstRow(pathToFolder + '/' + firstCSVName)
    columnsFromTable =       getColumnsFromTable(folder)

    if insertToTableColumnsNeeded(columnsFromFirstCSV, columnsFromTable, folder, firstCSVName) == 1:
        saveJsonWithColumns(columnsFromFirstCSV, folder)

def tableExists(tableName):
    connection = pymysql.connect(host='localhost',
                             user='root',
                             password='powerfi',
                             db='db',
                             charset='utf8mb4'
                            )
    try:
        with connection.cursor() as cursor:
            # Delete table with tableName
            # pdb.set_trace()
            result = cursor.execute(""" SHOW TABLES LIKE '%{0}%'; """.format(tableName))
            if result == 1:
                return True
            else:
                return False
            connection.commit()

    finally:
        connection.close()

def getColumnsFromTable(folder):
    columns = []
    try:
        with open('data.txt') as json_file:
            data = json.load(json_file)
            i=0
            for p in data[folder]:
                columns.append(p[''+str(i)+''])
                i+=1

    finally:
        return columns

# inserir na tabela colunas que ainda não existem
def insertToTableColumnsNeeded(columnsFromFirstCSV, columnsFromTable, tableName, csvPath):
    columnsToInsert = returnDifferentColumns(columnsFromFirstCSV, columnsFromTable)
    
    if len(columnsToInsert) != 0:
        schema = getAlterSchema(csvPath, tableName, columnsToInsert)
        # pdb.set_trace()

        connection  = pymysql.connect(host='localhost',
                                user='root',
                                password='powerfi',
                                db='db',
                                charset='utf8mb4'
                                )
        try:
            with connection.cursor() as cursor:
                cursor.execute(schema)
                connection.commit()
        finally:
            # pdb.set_trace()
            connection.close()
            return 1
    else:
        return 0


def getAlterSchema(csvPath, tableName, columnsToInsert):
    tupla = getArrayWithFirstRowAndSecondRowLenght(csvPath)
    tupla = getArrayWithFirstRowAndSecondRowLenghtToAlter(csvPath, tupla, columnsToInsert)
    schema = 'ALTER TABLE ' + tableName + ' ADD COLUMN '
    i = 0
    for row in tupla:
        # pega o nome da coluna e o tamanho máximo do campo
        maxLenght = int(row[0]) * 2
        if maxLenght < 25:
            maxLenght = 1000
        elif maxLenght > 200:
            maxLenght = 3000
        if i == 0:
            schema += ' ( ' + row[1]  + ' nvarchar('    + str(round(maxLenght)) + ')'
        else:
            schema += ', ' + row[1]  + ' nvarchar('    + str(round(maxLenght)) + ')'
        i += 1
    schema += ')'
    return schema

def getArrayWithFirstRowAndSecondRowLenghtToAlter(csvPath, tupla, columns):
    arrayLength = []
    arrayColumn = []
    for column in columns:
        for dupla in tupla:
            if column == dupla[0]:
                arrayLength.append(dupla[1])
                arrayColumn.append(dupla[0])
    # pdb.set_trace()

    arrayLength         = np.array(arrayLength)
    arrayColumn         = np.array(arrayColumn)
    array               = np.stack((arrayLength, arrayColumn), axis=-1)

    return array


def returnDifferentColumns(columnsFromFirstCSV, columnsFromTable):
    arrayToTableInsert = columnsFromFirstCSV
    # pdb.set_trace()
    # mudando ponteiro de referencia
    columnsFromFirstCSV     = tuple(columnsFromFirstCSV)
    columnsFromTable        = tuple(columnsFromTable)
    for csvElement in columnsFromFirstCSV:
        for tableElement in columnsFromTable:
            if csvElement == tableElement:
                arrayToTableInsert.remove(csvElement)

    return arrayToTableInsert

def saveJsonWithColumns(columnsToSave, folder):
    columnsToSave = showColumnsTable()
    # pdb.set_trace()
    data = {}
    data[folder] = []
    i=0
    for element in columnsToSave:
        data[folder].append({
            ''+str(i)+'': ''+ element +''
        })
        i += 1

    with open('data.txt', 'w') as outfile:
        json.dump(data, outfile)

def showColumnsTable():
    columnsInTable = []
    connection  = pymysql.connect(host='localhost',
                                user='root',
                                password='powerfi',
                                db='db',
                                charset='utf8mb4'
                                )
    try:
        with connection.cursor() as cursor:
            cursor.execute("SHOW columns FROM profile")
            for column in cursor.fetchall():
                columnsInTable.append(column[0])
    finally:
        connection.close()
        return columnsInTable



##### funções para pegar dados csv

def getData(csvPath):
    return pd.read_csv (csvPath)

# pega o dataFrame com os dados do csv
def getDataFrame(data, csvPath):
    i = 0
    numberOfLines = []
    for row in  getFirstRow(csvPath):
        numberOfLines.append(i)
        i += 1
    # o numberOfLines é para nomear todas as células da primeira coluna com números
    df = pd.DataFrame(data, numberOfLines)
    # substitui NaN por null
    df = df.replace(np.nan, 'null')
    return df


def  getFirstRow(csvPath):
    file = pd.read_csv(csvPath)
    columns = list(file.head(0))
    return columns

def  getSecondRow(csvPath):
    file = pd.read_csv(csvPath, nrows=1)
    columns = list(file)
    return file