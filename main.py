import flask, time, os
import pandas as pd
from flask import request

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['GET'])
def home():
    return "<h1>Power-Fi system to retrieve Skyfii Data</h1><p>Downloads updates from skyfii systems.</p>"

@app.route('/connected_now', methods=['POST'])
def connected_now():
    if request.method == 'POST':
        req_data = request.get_json()
        df = pd.DataFrame(req_data)
        df.to_csv(os.getcwd()+"/unprocessed/connected_now/"+str(time.time())+".csv", encoding='utf-8', index=False)
        return "<h1>Done!</h1>"

@app.route('/profile', methods=['POST'])
def profile():
    if request.method == 'POST':
        req_data = request.get_json()
        df = pd.DataFrame(req_data)
        df.to_csv(os.getcwd()+"/unprocessed/profile/"+str(time.time())+".csv", encoding='utf-8', index=False)

        return "<h1>Done!</h1>"

@app.route('/url', methods=['POST'])
def url():
    if request.method == 'POST':
        req_data = request.get_json()
        df = pd.DataFrame(req_data)
        df.to_csv(os.getcwd()+"/unprocessed/url/"+str(time.time())+".csv", encoding='utf-8', index=False)
        return "<h1>Done!</h1>"

@app.route('/visit', methods=['POST'])
def visit():
    if request.method == 'POST':
        req_data = request.get_json()
        df = pd.DataFrame(req_data)
        df.to_csv(os.getcwd()+"/unprocessed/visit/"+str(time.time())+".csv", encoding='utf-8', index=False)
        return "<h1>Done!</h1>"

@app.route('/end_access', methods=['POST'])
def end_access():
    if request.method == 'POST':
        req_data = request.get_json()
        df = pd.DataFrame(req_data)
        df.to_csv(os.getcwd()+"/unprocessed/end_access/"+str(time.time())+".csv", encoding='utf-8', index=False)
        return "<h1>Done!</h1>"

@app.route('/begin_access', methods=['POST'])
def begin_access():
    if request.method == 'POST':
        req_data = request.get_json()
        df = pd.DataFrame(req_data)
        df.to_csv(os.getcwd()+"/unprocessed/begin_access/"+str(time.time())+".csv", encoding='utf-8', index=False)
        return "<h1>Done!</h1>"

@app.route('/enter_area', methods=['POST'])
def enter_area():
    if request.method == 'POST':
        req_data = request.get_json()
        df = pd.DataFrame(req_data)
        df.to_csv(os.getcwd()+"/unprocessed/enter_area/"+str(time.time())+".csv", encoding='utf-8', index=False)
        return "<h1>Done!</h1>"

app.run(debug=False, host='0.0.0.0', port=int("80"))